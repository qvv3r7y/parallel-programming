________________________________________Find_Calls

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total
 time   seconds   seconds    calls   s/call   s/call  name
 98.82     29.53    29.53     8240     0.00     0.00  mul_matrix_vector
  0.33     29.63     0.10    32956     0.00     0.00  scalar_mul
  0.23     29.79     0.07    24717     0.00     0.00  mul_num_vector
  0.17     29.91     0.05    16478     0.00     0.00  add_vectors
  0.10     29.95     0.04     8240     0.00     0.00  check_end_alg
  0.10     29.98     0.03     8240     0.00     0.00  sub_vectors
  0.07     29.99     0.02        1     0.02     0.02  initial_alg
  0.00     29.99     0.00        1     0.00    29.99  do_magic
  0.00     29.99     0.00        1     0.00     0.00  dump_log
  0.00     29.99     0.00        1     0.00     0.00  free_data
  0.00     29.99     0.00        1     0.00     0.00  print_solution

_________________________________________W/O_parallel

mul_matrix: 30.258
scalar_mul: 0.122
mul_num_vector: 0.085
add_vectors: 0.063
check: 0.046
all: 30.681

_________________________________________parallel_for_MPI_v1

mul_matrix: 14.144
scalar_mul: 0.189
mul_num_vector: 0.122
add_vectors: 0.110
check: 0.089
all: 17.111


mul_matrix: 0.000
scalar_mul: 0.000
mul_num_vector: 0.000
add_vectors: 0.000
check: 0.000
all: 0.383

mul_matrix: 0.000
scalar_mul: 0.000
mul_num_vector: 0.000
add_vectors: 0.000
check: 0.000
all: 0.383

mul_matrix: 17.779
scalar_mul: 0.139
mul_num_vector: 0.096
add_vectors: 0.074
check: 0.054
all: 19.079

mul_matrix: 17.613
scalar_mul: 0.138
mul_num_vector: 0.095
add_vectors: 0.073
check: 0.053
all: 19.079

